export class Weather {
  constructor( public dateTime : number, public skyConditions : string , public airPressure  : number, public windSpeed  : number, public gustSpeed  : number){}
}

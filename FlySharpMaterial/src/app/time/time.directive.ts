import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[apptime]'
})
export class TimeDirective {

  constructor(el : ElementRef) {
    this.setStyle(el);
    this.showTime(el);
    setInterval(() => {
      this.showTime(el);
    }, 1000);
  }

  private showTime(el: ElementRef){
    let myDate =  new Date();
    el.nativeElement.innerHTML = myDate.toLocaleTimeString("en-US");
  }
  private setStyle(el: ElementRef){
    el.nativeElement.style.fontSize = '2em';
    el.nativeElement.style.marginTop = '0.2em';
    el.nativeElement.style.float = 'right';
  }

}

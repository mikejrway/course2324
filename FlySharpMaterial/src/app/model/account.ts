export interface Account {
  firstName: string,
  familyName: string,
  email: string,
  address1: string,
  address2: string,
  city: string,
  postCode: string
}
 

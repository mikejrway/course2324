import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display message saying "Special Offer of the month 10% off all round-the-World flights"', async () => {
    await page.navigateToTab('home');
    expect(await page.getParagraphText()).toEqual('Special Offer of the month 10% off all round-the-World flights');
  });

  it('should have an App-Home component', async () => {
    await page.navigateToTab('home');
    expect(await page.getAppHomeH1()).toEqual('Special Offer of the month 10% off all round-the-World flights');
  });

  it('should show 20 rows in the table', async () => {
    await page.navigateToTab('buy');
    expect(await page.getNumTableRows()).toEqual(20);
  });

  it('should show 9 columns in the table', async () => {
    await page.navigateToTab('buy');
    expect(await page.getNumTableCols()).toEqual(9);
  });

  it('flight number for 5th flight should be 114', async () => {
    await page.navigateToTab('buy');
    expect(await page.getTableCellData('5', '2')).toBe('114');
  });

  it('destination for 5th flight should be JFK', async () => {
    await page.navigateToTab('buy');
    expect(await page.getTableCellData('5', '4')).toBe('JFK');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

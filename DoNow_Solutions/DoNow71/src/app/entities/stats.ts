export interface Stats {
  date  : string,
  temperature : number;
  tempUnits : string;
}

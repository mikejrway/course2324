export class Preferences {
  constructor(
    public name: string,
    public location: string,
    public temperatureUnits: string,
    public speedUnits: string,
    public forecastLength: number

  ){}
}

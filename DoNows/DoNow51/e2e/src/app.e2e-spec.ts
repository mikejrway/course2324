import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Pseudo Keys Example');
  });


  it('should reflect test data in the #outputAll output area', async ()=>{
    let inputEle = null;
      debugger;
      page.navigateTo();
      inputEle = page.getInputElement();
      await inputEle.sendKeys("test data");
      expect(page.getOutputAllText()).toEqual("test data");
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

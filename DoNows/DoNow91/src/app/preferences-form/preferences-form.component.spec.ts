/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { PreferencesFormComponent } from './preferences-form.component';

describe('Component: PreferencesForm', () => {
  it('should create an instance', () => {
    let component = new PreferencesFormComponent();
    expect(component).toBeTruthy();
  });
});
